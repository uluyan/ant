const request = require('request');
const getCharset = require('../lib/charset');
const logger = require('../lib/logger')('test');

function testCharset(options, callback) {
    request(options, (err, res, body) => {
        if (err) {
            logger.error(err);
            callback('null');
            return null;
        }

        const charset = getCharset(body, (linkErr) => {
            if (linkErr) {
                logger.error(linkErr);
                return null;
            }
        });
        callback(charset);
    });
}

const charsetOptions = { uri: 'http://manage.mofcom.gov.cn/article/shehsj/' };

testCharset(charsetOptions, (charset) => {
    console.log(charset);
});
