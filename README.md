# ant
爬虫

## Files
```
├── configs.js
├── crawler.js
├── lib
│   ├── charset.js
│   ├── clean.js
│   ├── htmlParser.js
│   ├── linkExt.js
│   ├── link.js
│   ├── logger.js
│   ├── output.js
│   └── tools.js
├── package.json
├── README.md
├── svm
│   ├── data
│   │   ├── negative
│   │   │   └── xml
│   │   ├── out
│   │   ├── positive
│   │   │   └── xml
│   │   └── test
│   │       └── xml
│   ├── lib
│   │   ├── check.py
│   │   ├── fetures.py
│   │   ├── __init__.py
│   │   ├── log.py
│   │   ├── relief.py
│   │   ├── svmtools.py
│   │   ├── utils.py
│   │   └── xmltools.py
│   ├── task.py
│   └── train.sh
├── test
│   ├── charsetTest.js
│   ├── cleanTest.js
│   ├── htmlParserTest.js
│   └── linkExtTest.js
└── worker.js
```

## Dependency

- python modules
 - jieba
 - numpy

- libsvm `apt install libsvm-tools`

- Node.js

- Node modules `npm install`

## Start

- 将正例/负例/测试数据分别放到`./svm/data/negative|positive|test/xml`下

- 训练

```
cd svm
./train.sh
cd ..
```

- 把需要下载的网站按照`{部门}/{子部门}.csv`的形式放在./data目录下

- 运行爬虫 `node crawler.js`