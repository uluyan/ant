# -*- coding: utf-8 -*-
"""
   检查文件是否符合分类
"""
import os
import sys
import svm_tools
import utils


def check(file_name, model):
    """检查文件是否符合分类"""
    full_name = '/tmp/' + utils.get_fullname(file_name)
    # xml2scale
    scale_file = full_name + '.scale'
    with open(scale_file, 'w') as f:
        f.write(svm_tools.convert2libsvm(file_name, 1))
    # predict
    result_file = full_name + '.result'
    cmd = ('svm-predict {0} {1} {2}'.format(scale_file, model, result_file)
    with open(result_file, 'r') as f:
        return f.readlines()[0][:-1]

    # clean
    os.system('rm ' + scale_file)
    os.system('rm ' + result_file)


if __name__ == '__main__':
    print check(sys.argv[1], sys.argv[2])

